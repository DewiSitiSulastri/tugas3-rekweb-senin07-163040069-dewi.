<?php

require APPPATH . '/libraries/REST_Controller.php';

class Aksesoris extends REST_Controller
{

    public function __construct($config = "rest")
    {
        parent::__construct($config);
        $this->load->model("Aksesoris_model", "aksesoris");
    }

    public function index_get()
    {
        $id = $this->get('id');
        $cari = $this->get('cari');

        if($cari != " "){
            $aksesoris = $this->aksesoris->cari($cari)->result();
        }else if ($id == ' '){
            $aksesoris = $this->aksesoris->getData(null)->result();
        }else{
            $aksesoris = $this->aksesoris->getData($id)->result();
        }
        $this->response($aksesoris);
        // ketikan source code yang ada di modul
    }

    public function index_put()
    {
        $nama = $this->put('nama');
        $jenis = $this->put('jenis');
        $stok = $this->put('stok');
        $harga = $this->put('harga');
        $gambar = $this->put('gambar');
        $id = $this->put('id');
       
        $data = array(
            'nama' => $nama,
            'jenis' => $jenis,
            'stok' => $stok,
            'harga' => $harga,
            'gambar' => $gambar,
           
        );
        $update = $this->aksesoris->update('aksesoris', $data, 'id', $this->put('id'));

        if($update){
            $this->response(array('status' => 'success', 200));
        }else{
            $this->response(array('status' => 'fail', 502)); 
        }
        // ketikan source code yang ada di modul
    }

    public function index_post()
    {
        $nama = $this->post('nama');
        $jenis = $this->post('jenis');
        $stok = $this->post('stok');
        $harga = $this->post('harga');
        $gambar = $this->post('gambar');
       
        $data = array(
            'nama' => $nama,
            'jenis' => $jenis,
            'stok' => $stok,
            'harga' => $harga,
            'gambar' => $gambar,
        );
        $insert = $this->aksesoris->insert($data);

        if($insert){
            $this->response(array('status' => 'success', 200));
        }else{
            $this->response(array('status' => 'fail', 502)); 
        }
        // ketikan source code yang ada di modul
    }

    public function index_delete()
    {
        $id = $this->delete('id');

        $delete = $this->aksesoris->delete('aksesoris', 'id', $id);
        if($delete){
              $this->response(array('status' => 'success', 201));
        }else{
            $this->response(array('status' => 'fail', 502)); 
        }
       // ketikan source code yang ada di modul
    }
}