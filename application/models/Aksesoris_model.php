<?php

class Aksesoris_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

    public function getData($id = null){
        // ketikan source code yang ada di modul
        $this->db->select("*");
        $this->db->from("aksesoris");
        
        if ($id == null) {
            # code...
            $this->db->order_by('id', 'asc');
        } else {
            $this->db->where('id', 'id');
        }
        return $this->db->get();
    }

    public function cari($cari)
    {
        // ketikan source code yang ada di modul
        $this->db->select("*");
        $this->db->from("aksesoris");
        $this->db->like('nama', $cari);
        $this->db->or_like('jenis', $cari);
        $this->db->or_like('stok', $cari);
        $this->db->or_like('harga', $cari);
      

        return $this->db->get();
    }

    public function insert($data){
       // ketikan source code yang ada di modul
        $this->db->insert('aksesoris', $data);
        return $this->db->affected_rows();
    }

    public function update($table, $data, $par, $var) {
        // ketikan source code yang ada di modul
        $this->db->update($table, $data, array($par => $var));
        return $this->db->affected_rows();
    }
    
    public function delete($table, $par, $var){
       // ketikan source code yang ada di modul
        $this->db->where( $par, $var);
        $this->db->delete($table);
        return $this->db->affected_rows();
    }
}